# The motivation for this script is that I want my content to include
# typographically correct quotes and em dashes, but templating
# langaguages that handle that kind of thing automatically (i.e.
# Markdown) give up more control over the HTML than I'd like, and I
# don't trust myself to hand-encode the HTML entities consistently. This
# script checks all the pug files and tells me if I used:
# * ` - `, `--`, or `'` anywhere
# * `"` anywhere outside of what appears to be an HTML attribute

defmodule Typography do
  def check_text(text, line_no, col_no, in_attr, results) do
    cond do
      text == "" -> results
      String.starts_with?(text, "\n") ->
        check_text(
          String.slice(text, 3..-1//1),
          line_no + 1,
          1,
          in_attr,
          results
        )
      String.starts_with?(text, " - ") ->
        check_text(
          String.slice(text, 3..-1//1),
          line_no,
          col_no + 3,
          in_attr,
          [{"Em dash (use &mdash;)", line_no, col_no} | results]
        )
      String.starts_with?(text, "--") ->
        check_text(
          String.slice(text, 2..-1//1),
          line_no,
          col_no + 2,
          in_attr,
          [{"Em dash (use &mdash;)", line_no, col_no} | results]
        )
      # Quotes immediately following an = are assumed to be HTML
      # attributes, e.g. `href="/"`
      String.starts_with?(text, "=\"") && !in_attr ->
        check_text(
          String.slice(text, 2..-1//1),
          line_no,
          col_no + 2,
          true,
          results
        )
      String.starts_with?(text, "\"") ->
        new_results = if in_attr do
          results
        else
          [{"Curly quote (use &ldquo; or &rdquo;)", line_no, col_no} | results]
        end
        check_text(
          String.slice(text, 1..-1//1),
          line_no,
          col_no + 1,
          false,
          new_results
        )
    String.starts_with?(text, "'") ->
        check_text(
          String.slice(text, 1..-1//1),
          line_no,
          col_no + 1,
          false,
          [{"Curly single quote (use &rsquo;)", line_no, col_no} | results]
        )
      true ->
        check_text(
          String.slice(text, 1..-1//1),
          line_no,
          col_no + 1,
          in_attr,
          results
        )
    end
  end
end

file_paths = File.ls!("content") |> Enum.filter(&String.ends_with?(&1, ".pug"))
errors = Enum.reduce(file_paths, [], fn file_path, errors ->
  text = File.read!(Path.join("content", file_path))
  errors ++ (
    Typography.check_text(text, 1, 1, false, [])
    |> Enum.map(fn {desc, line_no, col_no} -> {file_path, desc, line_no, col_no}; end)
  )
end)

if errors != [] do
  for {file_path, desc, line_no, col_no} <- Enum.reverse(errors) do
    IO.puts(:stdio, "#{file_path}: #{desc} at #{line_no}:#{col_no}")
  end
  exit({:shutdown, 1})
end
