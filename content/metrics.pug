p The term <strong>Metrics</strong> refers to time series data: values that go up and (sometimes) down over time, typically indexed by resource, machine, and/or service. Metrics let you answer questions like &ldquo;what&rsquo;s the closest our database servers have come to exhausting physical memory?&rdquo; and &ldquo;how many user requests were serviced in the Asia-Pacific region yesterday?&rdquo;

p In some ways, metrics are limited: they hold only numeric values and they can be difficult to correlate with particular events or code paths. On the other hand, they&rsquo;re efficient to store, flexible to query, and great at predicting resource exhaustion. For example, if a storage volume&rsquo;s capacity was at 70% two hours ago, 80% one hour ago, and 90% just now, it&rsquo;s likely that your system is going to start having problems in an hour without intervention.

p
  strong Without good service metrics, your business may:
  ul
    li Be liable to miss predictable failure states, turning what would be simple maintenance tasks into costly downtime.
    li Have poor capacity planning, leaving the service overprovisioned on cloud resources or unable to keep up with peak load.

p This site uses <a href="https://prometheus.io">Prometheus</a> to collect metrics and <a href="https://grafana.com/grafana/">Grafana</a> to display them. Check out the <a href="https://metrics.ezrastevens.com">public dashboard</a> for a real-life example.

aside
  p <strong>Hot metrics tip: Use histograms.</strong> It&rsquo;s common for businesses to use percentiles to track performance metrics: <em>&ldquo;Are we keeping our 99th-percentile latency under 200 milliseconds?&rdquo;</em> However, percentiles are costly to compute and they don&rsquo;t compose well&mdash;a percentile measured yesterday and one measured today can&rsquo;t generally be combined into any kind of meaningful two-day aggregate.

  p Instead, turn the question sideways and ask &ldquo;Is the number of requests that get serviced in 200ms at least 99% as large as the total number of requests?&rdquo; This can be answered with two simple counters: one that gets incremented on each request, and one that gets incremented only on sufficiently fast requests. Unlike percentiles, counters compose trivially. To treat any number of servers as a single unit, add their respective counters together. To narrow the measurement to any time period, subtract the start-time value from the end-time value.

  p Add in some more counters&mdash;say, one each for 50ms, 100ms, 200ms, 400ms, and 800ms&mdash;and you have a histogram, suitable for a coarse-grained view into your application&rsquo;s performance curve. Further break them down by request type, server location, and any other factors you care about, and you can generate a nice heatmap of performance issues that can be sliced and filtered however you like. Percentiles can only dream of this much flexibility!
