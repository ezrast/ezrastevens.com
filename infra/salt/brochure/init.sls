# Sets up the Elixir app and supporting services.
# On a larger system, this would be split into multiple state files
# with looser coupling.

################ Set up the brochure site ################

curl: pkg.installed
unzip: pkg.installed

/opt/brochure:
  file.directory

/opt/brochure/update.sh:
  file.managed:
    - source: salt://brochure/update.sh

{% for env, interface in [('prod', '127.0.0.100'), ('stg', '127.0.0.200')] %}
brochure-{{ env }}:
  user.present:
    - system: true

/etc/systemd/system/brochure-{{ env }}.service:
  file.managed:
    - source: salt://brochure/brochure.service.jinja
    - template: jinja
    - context:
        env: {{ env }}
        interface: {{ interface }}
    - onchanges_in:
      - cmd: 'systemctl daemon-reload'
{% endfor %}

################ Caddy ################

# Install steps adapted from https://caddyserver.com/docs/install#debian-ubuntu-raspbian
debian-keyring: pkg.installed
debian-archive-keyring: pkg.installed
apt-transport-https: pkg.installed

/etc/apt/trusted.gpg.d/caddy-stable.asc:
  file.managed:
    - source: https://dl.cloudsmith.io/public/caddy/stable/gpg.key
    - source_hash: 5791c2fb6b6e82feb5a69834dd2131f4bcc30af0faec37783b2dc1c5c224a82a

/etc/apt/sources.list.d/caddy-stable.list:
  file.managed:
    - source: https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt
    - source_hash: 1be5a3361b53bfd7d520446192ec06d533d633f5d8fbe6300d52ffe2f56da438

caddy:
  pkg.installed:
    - refresh: true
    - require:
      - file: /etc/apt/trusted.gpg.d/caddy-stable.asc
      - file: /etc/apt/sources.list.d/caddy-stable.list
      - pkg: debian-keyring
      - pkg: debian-archive-keyring
      - pkg: apt-transport-https

/etc/caddy/Caddyfile:
  file.managed:
    - source: salt://brochure/Caddyfile
    - require:
      - pkg: caddy

caddy.service:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/caddy/Caddyfile

################ Prometheus ################

prometheus:
  user.present:
    - system: true

{% set prometheus_version = "2.35.0" %}
/opt/prometheus-{{ prometheus_version }}.linux-amd64.tar.gz:
  file.managed:
    - source: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus_version }}/prometheus-{{ prometheus_version }}.linux-amd64.tar.gz
    - source_hash: e4546960688d1c85530ec3a93e109d15b540f3251e1f4736d0d9735e1e857faf

extract prometheus:
  archive.extracted:
    - name: /opt
    - source: /opt/prometheus-{{ prometheus_version }}.linux-amd64.tar.gz
    - requires:
        - file: /opt/prometheus-{{ prometheus_version }}.linux-amd64.tar.gz

/opt/prometheus:
  file.symlink:
    - target: prometheus-{{ prometheus_version }}.linux-amd64

/var/lib/prometheus:
  file.directory:
    - user: prometheus

/etc/systemd/system/prometheus.service:
  file.managed:
    - source: salt://brochure/prometheus.service
    - onchanges_in:
      - cmd: 'systemctl daemon-reload'

/etc/prometheus.yml:
  file.managed:
    - source: salt://brochure/prometheus.yml

prometheus.service:
  service.running:
    - enable: True
    - requires:
      - user: prometheus
      - file: /etc/prometheus.yml
      - file: /etc/systemd/system/prometheus.service
      - file: /opt/prometheus
      - archive: extract prometheus
      - cmd: systemctl daemon-reload
    - watch:
      - file: /etc/prometheus.yml
      - file: /etc/systemd/system/prometheus.service

################ Node Exporter ################

node_exporter:
  user.present:
    - system: true

{% set node_exporter_version = "1.3.1" %}
/opt/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz:
  file.managed:
    - source: https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter_version }}/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz
    - source_hash: 68f3802c2dd3980667e4ba65ea2e1fb03f4a4ba026cca375f15a0390ff850949

extract node_exporter:
  archive.extracted:
    - name: /opt
    - source: /opt/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz
    - requires:
        - file: /opt/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz

/opt/node_exporter:
  file.symlink:
    - target: node_exporter-{{ node_exporter_version }}.linux-amd64

/etc/systemd/system/node_exporter.service:
  file.managed:
    - source: salt://brochure/node_exporter.service
    - onchanges_in:
      - cmd: 'systemctl daemon-reload'

node_exporter.service:
  service.running:
    - enable: True
    - reload: True
    - requires:
      - user: node_exporter
      - file: /etc/systemd/system/node_exporter.service
      - file: /opt/node_exporter
      - archive: extract node_exporter
      - cmd: systemctl daemon-reload
    - watch:
      - file: /etc/systemd/system/node_exporter.service

################ Misc ################

systemctl daemon-reload: cmd.run
