#!/bin/bash
# Downloads the latest artifacts from Gitlab and points staging at them.
# If I were getting paid for this, the artifacts would be versioned to
# make it easier to revert to an earlier release.
set -euxo pipefail

dirname="brochure-`date +%F_%H-%M-%S`"
mkdir "$dirname"

pushd "$dirname"
curl --output brochure.zip -L 'https://gitlab.com/api/v4/projects/35720384/jobs/artifacts/main/download?job=mix+release'
unzip brochure.zip
popd

ln -sf "$dirname" current-stg
systemctl restart brochure-stg
echo "Staging set to $dirname"
echo "To promote production:  ln -sf `readlink current-stg` current-prod && systemctl restart brochure-prod"
