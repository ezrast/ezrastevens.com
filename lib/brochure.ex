require EEx

defmodule Brochure do
  import Util
  use Plug.Router
  use Prometheus.Metric

  if Mix.env() == :dev do
    use Plug.Debugger
  end

  plug(Plug.Static,
    at: "/static",
    from: "public"
  )

  plug(:record_start)
  plug(:match)
  plug(:dispatch)
  plug(:record)

  def record_start(conn, _opts) do
    Plug.Conn.put_private(conn, :start_time, System.monotonic_time())
  end

  def record(conn, _opts) do
    geoip =
      Plug.Conn.get_req_header(conn, "x-forwarded-for")
      |> List.first()
      |> or_else(fn -> Map.get(conn, :ip, nil) end)
      |> and_then(&GeoIP.lookup(&1))

    country_code =
      case geoip do
        {:ok, geoip} -> geoip |> Map.get(:country)
        _ -> nil
      end

    continent = Countries.country_to_continent(country_code)

    route = Map.get(conn, :route, "#{conn.method} #{elem(conn.private.plug_route, 0)}")

    Histogram.observe(
      [
        name: :brochure_request_duration_seconds,
        labels: [route, conn.status, continent]
      ],
      System.monotonic_time() - conn.private.start_time
    )

    conn
  end

  get "/" do
    send_resp(conn, 200, Pages.home())
  end

  get "/about" do
    send_resp(conn, 200, Pages.about())
  end

  get "/metrics" do
    send_resp(conn, 200, Pages.metrics())
  end

  if Mix.env() == :dev do
    get "/dev" do
      content = """
      app_dir: #{Application.app_dir(:brochure)}
      """

      send_resp(conn, 200, content)
    end
  end

  match _ do
    send_resp(conn, 404, Pages.not_found()) |> Map.put(:route, "404")
  end
end

defmodule Pages do
  EEx.function_from_file(:defp, :layout, "content/layout.eex", [:path, :inner_content])

  def make_anchor(path, href, text) do
    if path == href do
      "<a class=\"current-page\" href=\"#{href}\">#{text}</a>"
    else
      "<a href=\"#{href}\">#{text}</a>"
    end
  end

  # Magically make a function for each .pug file in /content
  page_paths = File.ls!("content") |> Enum.filter(&String.ends_with?(&1, ".pug"))

  for path <- page_paths do
    full_path = "content/#{path}"
    @external_resource full_path
    inner_content =
      case File.read(full_path) do
        {:ok, pug} -> EEx.eval_string(Expug.to_eex!(pug))
        _ -> raise "Could not read: #{full_path}"
      end

    # drop .pug suffix
    page = String.slice(path, 0..-5//1)
    url_path = if page == "home", do: "/", else: "/#{page}"

    def unquote(String.to_atom(page))() do
      layout(unquote(url_path), unquote(inner_content))
    end
  end
end
