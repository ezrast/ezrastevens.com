defmodule Brochure.Application do
  use Application

  @impl true
  def start(_type, _args) do
    if !File.dir?("public") do
      raise("Expected static assets at ./public")
    end

    interface =
      case System.get_env("BROCHURE_INTERFACE") do
        nil ->
          raise "BROCHURE_INTERFACE must be set"

        interface_str ->
          case :inet.parse_address(to_charlist(interface_str)) do
            {:ok, addr} -> addr
            {:error, err} -> raise "Could not parse BROCHURE_INTERFACE: #{err}"
          end
      end

    port =
      case System.get_env("BROCHURE_PORT") do
        nil ->
          4001

        port_str ->
          case Integer.parse(port_str) do
            {int, ""} -> int
            _ -> raise "Could not parse BROCHURE_PORT"
          end
      end

    metrics_port =
      case System.get_env("BROCHURE_METRICS_PORT") do
        nil ->
          9998

        port_str ->
          case Integer.parse(port_str) do
            {int, ""} -> int
            _ -> raise "Could not parse BROCHURE_METRICS_PORT"
          end
      end

    children = [
      {Plug.Cowboy,
       scheme: :http,
       plug: Brochure,
       options: [
         ip: interface,
         port: port
       ]},
      {Plug.Cowboy,
       scheme: :http,
       plug: Metrics,
       options: [
         ip: interface,
         port: metrics_port
       ]}
    ]

    opts = [strategy: :one_for_one, name: Brochure.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

defmodule Metrics do
  use Prometheus.Metric

  def init(_opts) do
    Histogram.new(
      name: :brochure_request_duration_seconds,
      labels: [:route, :status, :continent],
      buckets: [0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1],
      help: "Http Request execution time"
    )
  end

  def call(conn, _opts) do
    Plug.Conn.send_resp(conn, 200, Prometheus.Format.Text.format())
  end
end
