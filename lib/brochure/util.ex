defmodule Util do
  def or_else(nil, fun), do: fun.()
  def or_else(obj, _fun), do: obj

  def and_then(nil, _fun), do: nil
  def and_then(obj, fun), do: fun.(obj)
end
