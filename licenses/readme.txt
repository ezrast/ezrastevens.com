This project is available under the MIT License, available at `LICENSE` in the project's root directory, with the following exception:

The Raleway font family, reproduced in part at `static/Raleway-VF.ttf`, is created by Matt McInerney and licensed under the SIL Open Font License, available at `licenses/OFL.md`. It is hosted at https://www.theleagueofmoveabletype.com/raleway.
