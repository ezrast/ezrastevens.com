defmodule Brochure.MixProject do
  use Mix.Project

  def project do
    [
      app: :brochure,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      application: [:prometheus_ex],
      extra_applications: [:logger],
      mod: {Brochure.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:expug, "~> 0.9"},
      {:geoip, "~> 0.2"},
      {:plug_cowboy, "~> 2.0"},
      {:prometheus_ex, "~> 3.0"}
    ]
  end
end
